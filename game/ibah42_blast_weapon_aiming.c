
#include "ibah42_blast_weapon_aiming.h"


void SaveEnemyOriginPosition( edict_t *_blastSourcer )
{
	VectorCopy ( _blastSourcer->enemy->s.origin, _blastSourcer->pos1 );	//save for aiming the shot
	_blastSourcer->pos1[2] += _blastSourcer->enemy->viewheight;
}

void CHICK_IfEnemyVisibleSaveItsPositionForAiming( edict_t *_blastSourcer )
{
	if ( visibleForMonstersWeapon( _blastSourcer, _blastSourcer->enemy, MZ2_CHICK_ROCKET_1 ) )
		SaveEnemyOriginPosition( _blastSourcer );
}

void TANK_IfEnemyVisibleSaveItsPositionForAiming( edict_t *_blastSourcer )
{
	if ( visibleForMonstersWeapon( _blastSourcer, _blastSourcer->enemy, MZ2_TANK_ROCKET_2 ) )
		SaveEnemyOriginPosition( _blastSourcer );
}

void SUPERTANK_IfEnemyVisibleSaveItsPositionForAiming( edict_t *_blastSourcer )
{
	if ( visibleForMonstersWeapon( _blastSourcer, _blastSourcer->enemy, MZ2_SUPERTANK_ROCKET_2 ) )
		SaveEnemyOriginPosition( _blastSourcer );
}

void GUNNER_IfEnemyVisibleSaveItsPositionForAiming( edict_t *_blastSourcer )
{
	if ( visibleForMonstersWeapon( _blastSourcer, _blastSourcer->enemy, MZ2_GUNNER_GRENADE_2 ) )
		SaveEnemyOriginPosition( _blastSourcer );
}

void IfEnemyVisibleSaveItsPositionForAiming( edict_t *_blastSourcer, int monsterModelOffset )
{
	if ( visibleForMonstersWeapon( _blastSourcer, _blastSourcer->enemy, monsterModelOffset ) )
		SaveEnemyOriginPosition( _blastSourcer );
}

qboolean visibleForMonstersWeapon(edict_t *monster, edict_t *itsEnemy, int offset)
{
	vec3_t	spot1;
	vec3_t	spot2;
	trace_t	trace;

	VectorCopy (monster->s.origin, spot1);
	spot1[2] += monster->viewheight;
	VectorCopy (itsEnemy->s.origin, spot2);
	spot2[2] += itsEnemy->viewheight;

	spot1[0] += monster_flash_offset[ offset ][0];
	spot1[1] += monster_flash_offset[ offset ][1];
	spot1[2] += monster_flash_offset[ offset ][2];

	trace = gi.trace (spot1, vec3_origin, vec3_origin, spot2, monster, MASK_OPAQUE);

	if (trace.fraction == 1.0)
		return true;
	return false;
}

float getCurrentVelocityAsInterpolated( float _maxV, float _minV, float _maxDist, float _minDist, vec3_t _realDist )
{
	// http://paulbourke.net/miscellaneous/interpolation/

	float length;
	float mu2, mu;
	float interpolatedVelocity;

	length =	_realDist[0] * _realDist[0]
			+	_realDist[1] * _realDist[1]
			+	_realDist[2] * _realDist[2]
	;

	length = sqrt ( length );


	if ( length > _maxDist )
		length = _maxDist;
	if ( length < _minDist )
		length = _minDist;

	mu = ( length - _minDist ) / ( _maxDist - _minDist );
	
	mu2 = ( 1.0f - cos( mu * 3.1415926f ) ) / 2.0f;
	interpolatedVelocity = _minV * ( 1.0f - mu2 ) + _maxV * mu2;

	return interpolatedVelocity;
}


float getCurrentVelocityAsInterpolated_forMonster( edict_t *self, float _maxV, float _minV, float _maxDist, float _minDist )
{
	vec3_t dirAim;
	VectorSubtract ( self->s.origin, self->enemy->s.origin, dirAim );
	return getCurrentVelocityAsInterpolated( _maxV, _minV, _maxDist, _minDist, dirAim );
}