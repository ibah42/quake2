#pragma once

#include "g_local.h"

void SaveEnemyOriginPosition( edict_t *_blastSourcer );

void IfEnemyVisibleSaveItsPositionForAiming( edict_t *_blastSourcer, int monsterModelOffset);

qboolean visibleForMonstersWeapon(edict_t *monster, edict_t *itsEnemy, int offset);

void CHICK_IfEnemyVisibleSaveItsPositionForAiming( edict_t *_blastSourcer );

void TANK_IfEnemyVisibleSaveItsPositionForAiming( edict_t *_blastSourcer );

void SUPERTANK_IfEnemyVisibleSaveItsPositionForAiming( edict_t *_blastSourcer );

void GUNNER_IfEnemyVisibleSaveItsPositionForAiming( edict_t *_blastSourcer );

float getCurrentVelocityAsInterpolated( float _maxV, float _minV, float _maxDist, float _minDist, vec3_t _realDist );


float getCurrentVelocityAsInterpolated_forMonster( edict_t *self, float _maxV, float _minV, float _maxDist, float _minDist );

